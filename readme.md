## Usage
 1. `npm install`
 2. Configure your test code
 3. Run with `node ./index.js`

## Contributions
If you add anything useful like running as a web app or CLI service, please submit a PR!