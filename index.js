const jsforce = require('jsforce');
const config = require('./config.json');

function chatter() {
	const sf = new jsforce.Connection({ loginUrl: config.loginUrl });
	const username = config.username;
	const password = config.password + config.token;

	sf.login(username, password, function (err, res) {
		if (err) { console.error(err); }

        console.log(res);
	});
};

chatter();